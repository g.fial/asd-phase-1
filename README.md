Objective:
- Scalable membership service (10,000 processes)
- The service must provide to each process, the Global view of all other processes.
- Build on top of a <b>Partial View</b> Membership Service and Information Dissemination System.
- Ensure that eventually:
	1. All correct processes are reported as part of the membership by every correct process.
	2. All failed processes aren't reported as part of the membership by every correct process.
- Build small app on top of our service that enables extracting the following information:
	1. View of local node about the composition of the global membership.
	2. Partial view composition in the lower layer.
	3. Number of messages sent/received by the information dissemination system.

Suggestions:
- Partial View Membership: Cyclon, Scamp or HyParView (or mix between them)
- Info. Diss. System: Gossip-Based alternative that avoids overloading, propagates fast and uses minimal network resources.
- Global Membership: Detect failures avoiding false positives. Heartbeat could be used here.
