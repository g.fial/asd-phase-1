Phase 1
Q: Quando o disconnect aleatorio atinge um processo que só tem o nó que se quer desconectar na vista? Segundo o algoritmo ele remove da ativa e põe na passiva, fica com a ativa com 0 elementos...
Q: No pseudo-codigo, diferença entre Procedure e Upon? Certas "Upon" parecem apenas funções (definidas no HyParView), como o dropRandomElementActiveView, não é chamado de fora.
Q: Não se pode usar mesmo o mecanismo de ator falhado do Akka para simular falhas detetadas pelo TCP do professor?
Q: Tamanho do Shuffle Random Walk?
Q (ERRO): No HyParView, não é considerado o problema de uma passive cheia. -> Corrigido.

Phase 2
Q: Como fazer função que dado valor unico produza hash único?

Q: Suponhamos que o objecto A produz um hash 210, ao escrever, o processo mais próximo é 190, portanto 190 guarda-o em si e nas suas réplicas. As escritas procuram o hash 210, chegam ao 190 e obtém o objecto. Imaginemos que um novo processo 200 apareçe, as leituras vão agora perguntar por A a este processo, e ele não o vai ter, portanto pergunta ao 190, correto?

Q: Supondo os processos (210 (master), 190 (replica), 180(replica)). Quando o master 210 morre, faz sentido 190 tornar-se master e procurar alguém para além seguinte ao 180 para se tornar a terceira réplica? Os processos 190, 180, 170 ficarão então com os conteúdos que iriam para a 210. Supondo que um novo processo 200 aparece, as leituras procuradas no 210 pesquisarão no 200, que não tem nada, como resolver isto? Deveremos perguntar ao proximo sucessivamente até encontrar? (por isto do master morrer e surgir alguém no meio pode acontecer sucessivamente) Não parece muito eficiente. Para resolver, faz sentido que quando surge um novo processo, se algum dos nossos objectos guardados tiver um hash mais proximo do novo nó, que o transfiramos para lá? Ou pelo menos enviar um apontador como no slide do Consistent Hashing ("Send to 200: Object A @ 190").

Q: Uma réplica de um processo anterior não replica esses mesmos conteúdos para as suas réplicas "pessoais"

Q: O que vai ter o storage service? Terá a estratégia do Paxos?
