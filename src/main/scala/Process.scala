import akka.actor.ActorSystem
import akka.actor.ExtendedActorSystem
import akka.actor.ActorSelection
import akka.actor.ActorRef
import akka.actor.Actor
import akka.actor.Props
import scala.concurrent.Await;
import scala.concurrent.duration.FiniteDuration
import java.util.concurrent.TimeUnit
import scala.io.StdIn
import java.io.File
import com.typesafe.config.ConfigFactory
import com.typesafe.config.Config
import akka.util.Timeout
import scala.concurrent.Future
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.Success
import scala.util.Failure
import akka.pattern.ask


object Process extends App {

  val PSIZE = 10;
  val ASIZE = 3;
  val ARWL = 4; // Active Random Walk
  val PRWL = 2; // Passive Random Walk
  val SRWL = 3; // Shuffle Random Walk
  val hb_time = 3 // in seconds
  val max_answer_time = 5 // tolerated limit to get an answer


  var system: ActorSystem = _
  var overlay_props: Props = _
  var gossip_props: Props = _
  var global_props: Props = _
  var tester_props: Props = _
  var contact_ref: String = null
  var process_name = ""
  
  case class TesterGlobalResponse(global_view: Set[String])
  case class TesterCrashedResponse(crashed : Set[String])  

  initialize

  // create network overlay layer as an Actor
  val overlay = system.actorOf(overlay_props, "overlay-layer")
  println(s"[Initializing] Created $overlay")
  
  val gossip = system.actorOf(gossip_props, "gossip-layer")
  println(s"[Initializing] Created $gossip")

  val global = system.actorOf(global_props, "global-layer")
  println(s"[Initializing] Created $global")
  
  val tester = system.actorOf(tester_props, "tester-layer")
  println(s"[Initializing] Created $tester")

  def initialize = {

    // if no arguments
    if(args.length < 1)
      printUsage

    process_name = args(0)

    // if contact server option (2nd argument) exists
    if(args.length == 2) {
      contact_ref = args(1)
      system = getActorSystem(process_name, false) // create contact-server seeking system
      println("[Initializing] Contact's Actor Reference: " + args(1))
    }
    // if this server should become the contact server
    else
      system = getActorSystem(process_name, true) // create contact-server system

    overlay_props = OverlayNetwork.props(process_name, contact_ref, ASIZE, PSIZE, ARWL, PRWL, SRWL) //TODO: Pass heartbeat time as a parameter
    gossip_props = Gossip.props(process_name)
    global_props = GlobalMembership.props(process_name, contact_ref != null, max_answer_time, hb_time)
    tester_props = ClientTester.props(process_name)
  }

  def printUsage = {
    println("To create a process:")
    println("SBT -> runMain Process <process_name> <contact_process>(optional)\n")
    println("<contact_process> should meet the following structure: akka.<protocol>://<actor system name>@<hostname>:<port>/<actor path>")
    System.exit(0)
  }

  def getActorSystem(name: String, becomeContact: Boolean): ActorSystem = {

    if(becomeContact) {
      val config = ConfigFactory.load.getConfig("ContactService")
      val system = ActorSystem(name, config)
      return system

    } else {
      val config = ConfigFactory.load.getConfig("LookupService")
      val system = ActorSystem(name, config)
      return system
    }
  }  
}
