import akka.actor.ActorRef
import akka.actor.Actor
import akka.actor.Props
import java.sql.Timestamp

object Gossip{
  
  def props(name: String): Props = {
    Props(new Gossip(name))
  }
  
  case class ActiveList(activeList: Set[String])
  case class Epidemic(msgType: String, senderNode: String, message: String, id: String) //TODO CHECK TYPE!
  case class MessagesDelivered(messagesNum: Int, uniqueMessages: Int)
}

class Gossip(name: String) extends Actor {
  import Gossip._
  
  val ANSI_RESET = "\u001B[0m"
  val ANSI_BLUE = "\u001B[34m"
  
  var deliveredNumber = 0
  var deliveredIDs = Set.empty[Int]
  var activeSet = Set.empty[String]
  
  override def receive = {
    
    case msg: ActiveList => {
      printlnColor("Received new active list: " + msg.activeList)
      updateActiveList(msg.activeList)
    }
    case msg: Epidemic => {
      receiveEpidemic(msg.msgType, msg.senderNode, msg.message, msg.id)
    }
    case msg: MessagesDelivered => {
      printlnColor("Received request to give message amount information.")
      printlnColor("Number of messages sent: " + deliveredNumber)
      printlnColor("Number of unique messages sent: " + deliveredIDs.size)
      sendMessagesInformation(msg.messagesNum, msg.uniqueMessages)
      printlnColor("Message sent.")
    }
    case "Teste de Ask!" => {
      sender ! "Ask and ye shall receive!"
    }
  }
  
  def updateActiveList(newActiveList: Set[String]) = activeSet = newActiveList
  
  def receiveEpidemic(msgType: String, senderNode: String, message : String, id: String){
    printlnColor("Received epidemic message: " + msgType + " sender: " + senderNode)
    var messageID = getUniqueMsgID(id)
    if(!isRepeated(messageID, deliveredIDs)){
      var selectedNodes = getAllNeighboursExceptSender(senderNode)
      printlnColor("senderNode: " + senderNode)
      printlnColor("active: " + activeSet)
      selectedNodes.foreach(node => {  
        sendToNeighbors(node, msgType, senderNode, message, id)
      })
      deliveredIDs += messageID
      printlnColor("ID = " + id)
      printlnColor("messageID = " + messageID)
      printlnColor("deliveredIDS = " + deliveredIDs)
    }
  }
  
  def sendToNeighbors(node : String, msgType: String, senderNode: String, message: String, id: String){
        var globalNode = node + "/user/global-layer"
        var gossipNode = node + "/user/gossip-layer"
        printlnColor("globalNode " + globalNode)
        printlnColor("gossipNode " + gossipNode)
        var nodeToSendGlobal = context.actorSelection(globalNode)
        var nodeToSendGossip = context.actorSelection(gossipNode)
        printlnColor("Sending to " + nodeToSendGlobal)
        printlnColor("With info " + msgType + " + " + senderNode + " + " + message)
        nodeToSendGlobal ! GlobalMembership.GossipReceive(msgType, senderNode, message)
        addMessageNumber()
        printlnColor("Sending to " + nodeToSendGossip)
        printlnColor("With info " + msgType + " + " + senderNode + " + " + message)
        nodeToSendGossip ! Gossip.Epidemic(msgType, senderNode, message, id)
        addMessageNumber()
        printlnColor("nodeGlobal: " + nodeToSendGlobal)
        printlnColor("nodeGossip: " + nodeToSendGossip)
  }
  
  def addMessageNumber() = deliveredNumber = deliveredNumber + 1
  
  def getAllNeighboursExceptSender(senderNode: String) : Iterator[String] = {
    var tempNode = senderNode.split("/user/global-layer")(0)
    activeSet.filter(_ != tempNode.split("/user/global-layer")(0)).iterator
  }
  
  def getUniqueMsgID(msg: String) : Int = msg.hashCode()
  
  def isRepeated(value: Int, set: Set[Int]) : Boolean = set.contains(value)
  
  def sendMessagesInformation(messagesNum : Int, uniqueMessages : Int) = {
    sender ! Gossip.MessagesDelivered(messagesNum, uniqueMessages)
    addMessageNumber()
  }
  
  def printlnColor(string: String) = println(ANSI_BLUE + string + ANSI_RESET)
}