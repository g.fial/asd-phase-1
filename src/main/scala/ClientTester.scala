import akka.actor.ActorSystem
import akka.actor.ExtendedActorSystem
import akka.actor.ActorSelection
import akka.actor.ActorRef
import akka.actor.Actor
import akka.actor.Props
import scala.concurrent.Future;
import scala.concurrent.Await;
import scala.concurrent.duration.FiniteDuration
import java.util.concurrent.TimeUnit
import scala.io.StdIn
import java.io.File
import com.typesafe.config.ConfigFactory
import com.typesafe.config.Config
import akka.util.Timeout
import scala.concurrent.Future
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.Success
import scala.util.Failure
import akka.pattern.ask

object ClientTester{
  
  def props(name: String): Props = {
    Props(new ClientTester(name))
  }
}

class ClientTester(name: String) extends Actor{

  override def receive = {
    case _ =>
  }
  
  println("-----------------------------------------------")
  println("Welcome to " + name + " tester layer.")
  println("-----------------------------------------------")
  var input = ""

  cliHelp()
  while(input != "exit"){
    try input = StdIn.readLine()
    catch {
      case t: Throwable => println("ERROR: Failed to get input")
      }
    input match {
      case "global" =>  println(askGlobalView(GlobalMembership.TesterGlobalView()))
      case "crashed" =>  println(askGlobalView(GlobalMembership.TesterGlobalCrashed()))
      case "help" | "?" => cliHelp()
      case "exit" => context.system.terminate()
      case x => println("Command \"" + x + "\" not found")
    }
    println()
  }
  
  def cliHelp(){
    println("global => Gives global view as seend by this process.")
    println("crashed => Gives set of crashed processes as seen by this process.")
    println("help => This info screen.")
    println("exit => Quits this terminal and closes the process.")
  }
  
  def askGlobalView(message: Any) = {
    implicit val timeout = Timeout(3 seconds)
    var result = Set.empty[String]
    val future: Future[Set[String]] = ask(context.system.actorSelection("/user/global-layer"), message).mapTo[Set[String]]
    future.onComplete{
      case Success(value) => println(value)
      case Failure(e) => println("ERROR: Failed to get response from GlobalView")
    }
  }
}