import akka.actor.{ActorSystem, ExtendedActorSystem, ActorRef, ActorSelection, Actor, Props, PoisonPill}
import akka.pattern.ask
import akka.util.Timeout
import com.typesafe.config.ConfigFactory
import com.typesafe.config.Config
import scala.concurrent.Future
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.Success
import scala.util.Failure
import scala.concurrent.Await;
import scala.concurrent.duration.FiniteDuration
import java.util.concurrent.TimeUnit

object OverlayNetwork {

  def props(name: String, contact: String, asize: Int, psize: Int, arwl: Int, prwl: Int, srwl: Int): Props = {
    Props(new OverlayNetwork(name, contact != null, contact, asize, psize, arwl, prwl, srwl))
  }
  //def props(name: String, asize: Int, psize: Int, arwl: Int, prwl: Int): Props = Props(new OverlayNetwork(name, false, null, asize, psize, arwl, prwl))

  case class Join()
  case class JoinReply()
  case class Forward(newNode: String, ttl: Int)
  case class Disconnect()
  case class Heartbeat()
  case class NeighborRequest(high: Boolean)
  case class NeighborReply(accepted: Boolean)
  case class Shuffle(source: String, ttl: Int, list: Set[String])
  case class ShuffleReply(list: Set[String])

}

class OverlayNetwork(name: String, hasContact: Boolean, contact: String, asize_limit: Int, psize_limit: Int, arwl: Int, prwl: Int, srwl: Int) extends Actor {
  import OverlayNetwork._
  
  var active = new HeartbeatedProcessView(asize_limit, Duration(15, SECONDS))
  var passive = new ProcessView(psize_limit)
  val selfAddress = getSelfAddress
  var shuffled, rejecters = Set[String]()
  var Ka = 2
  var Kp = 3

  override def preStart(): Unit = {
    
    if (hasContact) {
      getActorSelection(contact) ! OverlayNetwork.Join()
      active.add(contact)
      notifyUpdateToActive()
    }
    
    println("\n\n\n CONNECT TO: " + selfAddress + "\n\n")

    context.system.scheduler.schedule(1 seconds, 3 seconds)(heartbeat)
    context.system.scheduler.schedule(30 seconds, 30 seconds)(initShuffle)

  }

  override def receive = {

    case msg: Join => {
      println(s"[Join] from $sender")
      receiveJoin()
      printMemory
    }
    case msg: Forward => {
      println(s"[Forward] from $sender")
      receiveForwardJoin(msg.newNode, msg.ttl)
      println()
      printMemory
    }
    case msg: Disconnect => {
      println(s"[Disconnect] from $sender")
      disconnect
      println()
      printMemory
    }
    case msg: NeighborRequest => {
      println(s"[Neighbor Request] from $sender")
      receiveNeighbor(msg.high)
      println()
      printMemory
    }
    case msg: NeighborReply => {
      print(s"[Neighbor Reply] from $sender, ")
      receiveNeighborReply(msg.accepted)
      println()
      printMemory
    }
    case msg: Shuffle => {
      println(s"[Shuffle] from $sender.")
      receiveShuffle(msg.source, msg.ttl, msg.list)
      println()
      printMemory
    }    
    case msg: ShuffleReply => {
      print(s"[Shuffle Reply] from $sender. ")
      receiveShuffleReply(msg.list)
      println()
      printMemory
    }
    case msg: Heartbeat => active.updateTimestamp(sender.path.toString)
    case msg: JoinReply => {
      println(s"[Join Reply] From $sender.")
      addNodeToActive(sender.path.toString)
      println()
      printMemory
    }
  }

  def addNodeToActive(node: String) = {
    if (node != selfAddress && !active.contains(node)) {
      if(active.isFull())
        dropRandomActiveElement
      active.add(node)
      notifyUpdateToActive()
    }
  }

  def addNodeToPassive(node: String) = {
    if (node != selfAddress && !active.contains(node) && !passive.contains(node)) {
      if(passive.isFull()){
        passive.removeRandom()
      }
      passive.add(node)
    }
  }

  def dropRandomActiveElement = {
    var randomElement = active.removeRandom()
    notifyUpdateToActive()
    getActorSelection(randomElement) ! OverlayNetwork.Disconnect()
    addNodeToPassive(randomElement)
  }

  def disconnect = {
    if(active.contains(sender.path.toString)){
      println(s"[Disconnect] removed $sender.path.toString")
      active.remove(sender.path.toString)
      notifyUpdateToActive()
      addNodeToPassive(sender.path.toString)
      if(active.isEmpty)
        sendNeighborRequest()
    }
    else
      println(s"[Disconnect] ERROR: $sender.path.toString NOT REMOVED!")
    
  }

  def heartbeat = {
    active.getUnresponsive.foreach(x => removeUnresponsive(x))
    active.iterator.foreach(x => getActorSelection(x) ! OverlayNetwork.Heartbeat())
  }

  def removeUnresponsive(node: String) = {
    active.remove(node)
    getActorSelection("/user/global-layer") ! GlobalMembership.FailureNotification(node)
    println("Removed Unresponsive: " + node)
    sendNeighborRequest()
    notifyUpdateToActive()
  }

  def receiveNeighbor(high_priority: Boolean) = {
    if(high_priority){
      addNodeToActive(sender.path.toString)
      sender ! OverlayNetwork.NeighborReply(true)
      print("[Neighbor Request] accepted (HIGH priority)")
    } else {
      if(active.isFull()) {
        sender ! OverlayNetwork.NeighborReply(false)
        print("[Neighbor Request] rejected (LOW priority)")
      }
      else{
        addNodeToActive(sender.path.toString)
        sender ! OverlayNetwork.NeighborReply(true)
        print("[Neighbor Request] accepted (LOW priority)")
      }
    }
    println(", sent Reply.")
  }
  
  def receiveNeighborReply(accepted: Boolean) = {
    if(!accepted){
      rejecters += sender.path.toString
      print(" did not accept. Request again? ")
      if(!active.isFull) {
        sendNeighborRequest()
        println("Yes.")
      } else { print("No.") }
    } else {
      println(" accepted.")
      passive.remove(sender.path.toString)
      addNodeToActive(sender.path.toString)
    }
  }

  def sendNeighborRequest() = {
    var found = false
    var q = passive.getRandomExceptSet(rejecters)
    while(!found && q != null) {
      if(!testConnection(q)) {
        passive.remove(q)
        println("[Outbound Neighbor Request] " + q + " removed (no connection)")
      }else {
        val priority = active.isEmpty
        getActorSelection(q) ! OverlayNetwork.NeighborRequest(priority)
        println("[Outbound Neighbor Request] To " + q + ". High priority? " + priority)
        found = true
      }
      if(!found)
        q = passive.getRandomExceptSet(rejecters)
    }
    rejecters = rejecters.empty
  }

  
  def receiveJoin() = {
    println("[Join] From " + sender.path.toString)
    addNodeToActive(sender.path.toString)
    active.iterator.filter(_ != sender.path.toString).foreach(x => getActorSelection(x) ! OverlayNetwork.Forward(sender.path.toString, arwl))
  }
  
  def receiveForwardJoin( newNode: String, ttl: Int ) = {
    if( ttl == 0 || active.size() == 1 ) {
      addNodeToActive(newNode)
      println(s"[Forward] added $newNode to active view and set JOIN REPLY")
      getActorSelection(newNode) ! OverlayNetwork.JoinReply()
    } else {
      if ( ttl == prwl ) {
        addNodeToPassive(newNode)
        println(s"[Forward] added $newNode to passive view")
      }
      val forwardTo = active.getDifferentRandom(newNode)
      getActorSelection(forwardTo) ! OverlayNetwork.Forward(newNode, ttl - 1)
      println(s"[Forward] forwarding $newNode to $forwardTo")
    }
  }

  def testConnection(node: String): Boolean = {
    implicit val timeout = Timeout(3 seconds)
    var result = false
    val future: Future[String] = ask(getActorSelection(node), "test connection").mapTo[String]
    future.onComplete{
      case Success(value) => result = true
      case Failure(e) => result = false
    }
    return result
  }

  def initShuffle {
    var setKa = active.pickRandom(Ka)
    var setKp = passive.pickRandom(Kp)
    var set = setKa ++ setKp + selfAddress
    var node = active.getDifferentRandom(selfAddress)
    if(!set.isEmpty && node != null) {
      getActorSelection(node) ! OverlayNetwork.Shuffle(selfAddress, srwl, set)
      shuffled = set
      var print_set = Set[String]()
      set.foreach(x => print_set += getActorName(x))
      println("\n[" + name + " Shuffle Init] Sending " + print_set)
    }
    else
      println("[" + name + " Shuffle Init] No conditions to start")
  }
  
  def receiveShuffle(source: String, ttl: Int, list: Set[String]){
    if(ttl > 0 && active.size() > 1){
      var node = active.getDifferentRandom(sender.path.toString)
      getActorSelection(node) ! OverlayNetwork.Shuffle(source, ttl-1, list)
      print("[Shuffle] forwarding " + getActorName(source) + "'s Shuffle to " + getActorName(node) + " with " + list)
    }
    else if(selfAddress != source){
      var toSend = passive.pickRandom(list.size)
      getActorSelection(source) ! OverlayNetwork.ShuffleReply(toSend)
      print("[Shuffle] accepted. Replying with " + toSend + ". ")
      shuffle(list, toSend)
    }
    else
      print("[Shuffle] dropping Shuffle, landed on Self.")
  }
  
  def receiveShuffleReply(list: Set[String]){
    shuffle(list, shuffled)
    shuffled = shuffled.empty
  }
  
  def shuffle(received: Set[String], sent: Set[String]) = {
    var r = scala.util.Random
    var passiveSet = passive.toSet
    var new_nodes = received &~ (Set[String](selfAddress) | active.toSet | passiveSet)
    var excess = new_nodes.size - (psize_limit - passive.size)
    if(excess > 0){
      var intersected = passiveSet & sent
      while (excess > 0 && intersected.size > 0) {
        val n = intersected.toVector(r.nextInt(intersected.size))
        passive.remove(n)
        excess = new_nodes.size - (psize_limit - passive.size)
      }
      while (excess > 0) {
        passive.removeRandom
        excess = new_nodes.size - (psize_limit - passive.size)
      }
    }
    new_nodes.foreach(x => passive.add(x))
    var print_set = Set[String]()
    new_nodes.foreach(x => print_set += getActorName(x))
    print("Added " + new_nodes + " to passive.")
  }

  def getActorRef(reference: String): ActorRef = {
    val timeout = new FiniteDuration(5, TimeUnit.SECONDS)
    val future = getActorSelection(reference).resolveOne(timeout)
    val contactRef = Await.result(future, timeout)
    println("Extracted actor reference: " + contactRef)
    contactRef
  }

  def getSelfAddress(): String = {
    val myAddress = context.system.asInstanceOf[ExtendedActorSystem].provider.getDefaultAddress
    val ipport = myAddress.host.get + ":" + myAddress.port.get
    val str = "akka.tcp://" + name + "@" + ipport + "/user/overlay-layer"
    str
  }

  def getActorName(actor: String): String = {
    actor.split("/user")(0).split("@")(0).split("://")(1)
  }

  def getActorSelection(address: String): ActorSelection = {
    context.system.actorSelection(address)
  }

  
  //TODO Gossip Stuff
  def gossipTests() = {
      notifyUpdateToActive()
      //gossipSendEpidemic()
      //Future test
      implicit val timeout = Timeout(5 seconds)
      val future: Future[String] = ask(getActorSelection("/user/gossip-layer"), "Teste de Ask!").mapTo[String]
      future.onComplete{
          case Success(value) => println("\u001B[31m" +"Got SUCCESS! " + value + "\u001B[0m")
          case Failure(e) => println("\u001B[31m" + "Got fail..." + "\u001B[0m")
        }
  }
  
  private def notifyUpdateToActive() = { // TODO: Add to pseudo code
    var base_forms = Set[String]()
    active.toSet().foreach(x => base_forms += x.split("/user/overlay-layer")(0))
    getActorSelection("/user/gossip-layer") ! Gossip.ActiveList(base_forms)
    getActorSelection("/user/global-layer") ! GlobalMembership.ActiveUpdate(base_forms)
  }

  private def printMemory() = println("Active: " + active.toString() + "\nPassive: " + passive.toString())

  private def actorSetToString(list: Set[String]) = {
    var str = ""
    list.foreach(x => str += (getActorName(x) + " "))
    str
  }
  
  /*def gossipSendEpidemic() = {
    getActorSelection("/user/gossip-layer") ! Gossip.Epidemic("Epidemic message from overlay", java.time.LocalDateTime.now.toString())
  }*/
  
}


class ProcessView(limit: Int) {
  import scala.collection.mutable.Stack

  case class ProcessViewException(message: String) extends Exception

  val r = scala.util.Random
  var contents = new Array[Tuple2[String, Duration]](limit)
  var free_positions = Stack[Int]()

  free_positions ++= (0 until limit).iterator

  // returns an iterator of this view
  def iterator(): Iterator[String] = {
    toSet.iterator
  }

  def toSet(): Set[String] = {
    var s = Set[String]()
    contents.filter(_ != null).foreach(x => s += x._1)
    s
  }

  // adds the actor to the view.
  def add(entry: String): Int = synchronized {
    if(free_positions.isEmpty)
      throw new ProcessViewException("Error adding to the view.")
    val pos = free_positions.pop
    contents(pos) = Tuple2[String, Duration](entry, null)
    return pos
  }
  
  // returns the size of view
  def size(): Int = {
    limit - free_positions.size
  }
  
  // checks if the view is full
  def isFull(): Boolean = {
    size() == limit
  }

  def isEmpty(): Boolean = {
    size() == 0
  }

  // checks if the actor is contained in the view
  def contains(entry: String): Boolean = {
    contents.exists(x => if(x != null) {x._1 == entry} else false)
  }

  // returns the index position of the given actor in the view
  def indexOf(entry: String): Int = {
    contents.indexWhere(x => if(x != null) {x._1 == entry} else false)
  }

  // get a random node different from the one passed as a parameter
  def getDifferentRandom(node: String): String = {
    val contained = contains(node)
    var rindex: Int = -1
    if(isEmpty || size() == 1 && contained)
      return null
    if(contained) {
      var temp = removeIndex(indexOf(node))
      rindex = getRandomIndex
      add(temp)
    } else
      rindex = getRandomIndex
    return getIndex(rindex)
  }

  def remove(node: String): Boolean = synchronized {
    val pos = indexOf(node)
    if(pos != -1) {
      removeIndex(pos) != null
    } else
      false
  }

  // get random node not present in the given set
  def getRandomExceptSet(set: Set[String]): String = synchronized {
    var temp = Vector[String]()
    contents.filter(x => x != null && !set.contains(x._1)).foreach(x => temp = temp :+ x._1)
    if(temp.isEmpty)
        return null
    temp(r.nextInt(temp.length))
  }

  // pick n random elements from this view
  def pickRandom(n: Int): Set[String] = synchronized {
    var picked = Set[String]()
    for(x <- 0 until n) {
      val pick = getRandomExceptSet(picked)
      if(pick == null)
        return picked
      picked += pick
    }
    return picked
  }

  // removes the node at the given index and returns it
  def removeIndex(pos: Int): String = synchronized {
    var node = contents(pos)._1
    contents(pos) = null
    free_positions.push(pos)
    return node
  }

  // removes a random element and returns the element
  def removeRandom(): String = synchronized {
    var pos = findConcretePos(r.nextInt(size()))
    return removeIndex(pos)
  }

  // gets array position from a random element
  def getRandomIndex(): Int = {
    findConcretePos(r.nextInt(size()))
  }

  // get element in a given position
  def getIndex(pos: Int): String = {
    contents(pos)._1
  }

  // prints a state of this data structure
  override def toString(): String = {

    def printEntry(entry: Tuple2[String, Duration]): String = {
      if(entry != null)
        return "  \u2022 " + (entry._1).toString.split("@")(0).split("://")(1) + "\n"
      else
        return ("  \u2022 < Empty Slot >\n")
    }

    var output: String = ""
    output += "ProcessView:\n"
    contents.foreach(x => output += printEntry(x))
    output += ("Capacity: " + size() + " / " + limit + "\n")
    return output
  }

  // finds the actual array pos for the pos-th element
  private def findConcretePos(pos: Int): Int = {
    var concretePos = -1
    for(_ <- 0 to pos) {
      concretePos += 1
      while(contents(concretePos) == null) {
	concretePos += 1
        if(concretePos == limit) // out of bounds
          throw new ProcessViewException("Relative position unmatchable with a concrete position: Reached end of the array (limit = " + limit + ") when looking for the " + pos + "º element.")
      }
    }
    return concretePos
  }
}

class HeartbeatedProcessView(limit: Int, timelimit: Duration) extends ProcessView(limit) {

  // adds the actor to the view.
  override def add(entry: String): Int = synchronized {
    if(free_positions.isEmpty)
      throw new ProcessViewException("Error adding to the view.")
    val pos = free_positions.pop
    contents(pos) = Tuple2[String, Duration](entry, currentTime)
    return pos
  }

  // returns set of nodes who failed to heartbeat within this duration
  def getUnresponsive(): Set[String] = {
    if(isEmpty)
      return Set[String]()
    val currtime = currentTime()
    var s = Set[String]()
    contents.filter(_ != null).foreach( {
      x => if(currtime - x._2 > timelimit) s += x._1
    })
    return s
  }

  // updates the timestamp value of the given node to current time
  def updateTimestamp(node: String) = synchronized {
    val pos = indexOf(node)
    if(pos != -1)
      contents(pos) = Tuple2[String, Duration](node, currentTime)
    else
      println("[Heartbeat Unknown] From " + node)
  }

  private def currentTime(): Duration = {
    var duration = Duration(System.currentTimeMillis, MILLISECONDS)
    duration
  }

  // prints a state of this data structure
  override def toString(): String = {
    val ctime = currentTime
    def printEntry(entry: Tuple2[String, Duration]): String = {
      if(entry != null)
        return ("  \u2022 [aged] " + (ctime - entry._2) + ": " + (entry._1).toString.split("@")(0).split("://")(1) + "\n")
      else
        return ("  \u2022 < Empty Slot >\n")
    }

    var output: String = ""
    output += "ProcessView:\n"
    contents.foreach(x => output += printEntry(x))
    output += ("Capacity: " + size() + " / " + limit + "\n")
    return output
  }
}

