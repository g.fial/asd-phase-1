import akka.actor.{ActorSystem, ExtendedActorSystem, ActorSelection, Actor, Props, Cancellable}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import akka.pattern.ask

object GlobalMembership {

  def props(name: String, hasContact: Boolean, max_answer_time: Int, hb_time: Int): Props = {
    Props(new GlobalMembership(name, hasContact, Duration(max_answer_time, SECONDS), Duration(hb_time, SECONDS)))
  }

  case class GlobalRequest()
  case class GlobalReply( list: Set[String] )
  case class PotentialFailure()
  case class FailureReply( still_alive_node: String )
  case class NodeFailure()
  case class NewNode()
  case class ActiveUpdate( list: Set[String] )
  case class FailureNotification( node: String )
  case class GossipReceive(msgType: String, sender: String, message: String)
  case class TesterGlobalView()
  case class TesterGlobalCrashed()
}

class GlobalMembership(name: String, hasContact: Boolean, max_answer_time: FiniteDuration, hb_time: FiniteDuration) extends Actor {
  import GlobalMembership._
  
  val selfAddress = getSelfAddress()
  var global_view = Set.empty[String]
  var global_received = false
  var active = Set.empty[String]
  var crashed = Set.empty[String]
  var awaiting_global = Set.empty[String]
  var potential = Set.empty[String]
  var request_view: Cancellable = _

  override def preStart() = {
    
    if(hasContact){
       request_view = context.system.scheduler.schedule(max_answer_time, max_answer_time)(sendGlobalViewRequest())
       context.system.scheduler.scheduleOnce(Duration(1, SECONDS))(gossipBroadcast("NEW NODE", selfAddress, "New node request"))
    }  
  }

  private def sendGlobalViewRequest() = {
    if(!active.isEmpty) {
      val node = getActorSelection(active.toIndexedSeq(scala.util.Random.nextInt(active.size)))
      node ! GlobalMembership.GlobalRequest()
    }
  }
  
  override def receive = {
    case msg: GlobalRequest => receiveGlobalRequest()
    case msg: GlobalReply => receiveGlobalReply(msg.list) 
    case msg: FailureReply => potential -= msg.still_alive_node
    case msg: ActiveUpdate => {
      active = active.empty
      msg.list.foreach(x => active += x + "/user/global-layer")
      }
    case msg: FailureNotification => context.system.scheduler.scheduleOnce(hb_time)(notifyPotentialFailure(msg.node))
    case msg: GossipReceive =>{
      gossipDeliver(msg.msgType, msg.sender, msg.message)
    }
    case msg: TesterGlobalView => {
      sender ! getTestGlobalView()
    }
    case msg: TesterGlobalCrashed => {
      sender ! getTestGlobalCrashed()
    }
    //case msg: NodeFailure =>
    //case msg: NewNode => 
    //case msg: PotentialFailure =>
  }
  
  def getTestGlobalCrashed() : Set[String] = {
    crashed
  }
  def getTestGlobalView() : Set[String] = {
    global_view
  }
  
  def receiveGlobalRequest() = {
    if(global_received)
      sender ! GlobalReply(global_view)
    else
      awaiting_global += sender.path.toString
  }

  def notifyPotentialFailure(node: String) = {
        println("notifyPotentialFailure")
    potential += node
    gossipBroadcast("POTENTIAL FAILURE", selfAddress, node)
    context.system.scheduler.scheduleOnce(max_answer_time)(submitCrashedNode(node))
  }

  def submitCrashedNode(node: String) = {
    println("submitCrashedNode")
    if (potential.contains(node)) {
      crashed += node
      potential -= node
      gossipBroadcast("NODE FAILURE", selfAddress, node)
    }
  }
  
  def receiveGlobalReply(list : Set[String]) = {
    if(request_view != null)
      request_view.cancel()
    global_received = true
    list.foreach(global_view += _)
    awaiting_global.foreach(n => getActorSelection(n) ! GlobalMembership.GlobalReply(global_view))
    global_view = global_view &~ crashed
  }
  
  def gossipBroadcast(msgType: String, sender: String, message: String) = {
    val time = java.time.LocalDateTime.now.toString()
    getActorSelection("/user/gossip-layer") ! Gossip.Epidemic(msgType, sender, message, time)
  }
  
  def gossipDeliver(msgType: String, sender: String, message: String) = {
    println("Received msgType " + msgType)
    if(msgType == "NEW NODE"){
      println("global_view = " + global_view + " + sender " + sender)
      global_view += sender
      println("Result = " + global_view)
      if(active.contains(sender))
        getActorSelection(sender) ! GlobalMembership.GlobalReply(active)
    }
    if(msgType == "POTENTIAL FAILURE"){
      if(active.contains(message) || message.equals(selfAddress))
        getActorSelection(sender) ! GlobalMembership.FailureReply(message)
    } 
    if(msgType == "NODE FAILURE"){
      crashed += message
      global_view -= message
    }
  }

  def getSelfAddress(): String = {
    val myAddress = context.system.asInstanceOf[ExtendedActorSystem].provider.getDefaultAddress
    val ipport = myAddress.host.get + ":" + myAddress.port.get
    val str = "akka.tcp://" + name + "@" + ipport + "/user/global-layer"
    str
  }

  def getActorSelection(address: String): ActorSelection = {
    context.system.actorSelection(address)
  }
}
