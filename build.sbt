name := "ASD Phase 1"
 
version := "1.0"
 
scalaVersion := "2.11.8"
 
libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % "2.4.10",
  "com.typesafe.akka" %% "akka-remote" % "2.4.10"
)

resolvers += "Akka Snapshot Repository" at "http://repo.akka.io/snapshots/"
